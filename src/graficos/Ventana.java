package graficos; // paquete de graficos


// Impotaciones
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JFrame;

import control.Teclado;
//fin

// clase gestora de la ventana en el sistema operativo
public class Ventana extends Canvas{
	
	private static final long serialVersionUID = 1L;
	
	// clases auxiliares
	private static JFrame ventana;
	
	//CONSTRUCTOR
	public Ventana(int ANCHO,int ALTO,String NOMBRE,Teclado teclado) {

		setPreferredSize(new Dimension(ANCHO,ALTO)); //  Dimensiones de la ventana		
		addKeyListener(teclado);
		// Propiedades de la ventana
		ventana = new JFrame(NOMBRE);							//NOMBRE
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		ventana.setResizable(false);								//¿CAMBIA DE TAMAÑO? = no
		ventana.setLayout(new BorderLayout());
		ventana.add(this,BorderLayout.CENTER);
		ventana.pack();
		ventana.setLocationRelativeTo(null);
		ventana.setVisible(true);								//¿ES VISIBLE? = si
	}
	public void paint(Graphics g) {
		g.fillRect(0, 0, 800, 600);
		
		g.setColor(Color.cyan);
		g.drawLine(0, 0, 800, 600);
		g.drawLine(800, 0, 0, 600);
		
	}
	public void setTitle(String s) {
		ventana.setTitle(s);
	}
}
