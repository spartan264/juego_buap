package juego_buap; // paquete principal de el juego


import control.Teclado;
// importaciones
import graficos.Ventana;
// fin

/**
 * @author gugama
 * @email spartan102gamez@gmail.com
*/

// clase principal del juego incluye metodo main
public class Juego implements Runnable{	
	
	
	// ANCHO y ALTO de la ventana	
	private static final short  ANCHO = 800; 
	private static final short  ALTO = 600;  
	
	// NOMBRE de el Juego
	private static final String NOMBRE = "NOMBRE";
	
	// VARIABLES
	private boolean enFuncionamiento = false;
	
	private int aps = 0;
	private int fps = 0;
	
	// clases Auxiliares
	private static Ventana ventana;
	private static Thread thread;
	private static Teclado teclado;
	
	// CONSTRUCTOR
	public Juego() {
		ventana = new Ventana(ANCHO, ALTO, NOMBRE, teclado);
	}
	// METODOS
	private void iniciar() {
		enFuncionamiento = true;
		thread = new Thread(this,"graficos");
		thread.run();
	}
	private void detener() {
		enFuncionamiento = false;
		
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	private void actualizar() {
		aps++;
	}
	private void dibujar() {
		fps++;
	}
	
	@Override
	public void run() {
		final int NS_PS = 1000000000;
		final int APS_OBJ = 60;
		final double NS_PA = NS_PS / APS_OBJ;
		
		long refContador = System.nanoTime();
		long refActualizacion = System.nanoTime();
		
		double tt = 0;
		double delta = 0;
		
		while(enFuncionamiento) {
			final long inicioBucle = System.nanoTime();
			
			tt = inicioBucle - refActualizacion;
			refActualizacion = inicioBucle;
			
			delta += tt / NS_PA;
			
			while(delta >= 1) {
				actualizar();
				delta--;
			}
			dibujar();
			
			if (System.nanoTime() - refContador > NS_PS) {
				ventana.setTitle(NOMBRE + " || aps: "+aps+" || fps: "+fps);
				aps =0;
				fps =0;
				
				refContador = System.nanoTime();
			}
		}
		
		
	}
	
	
	// CLASE MAIN
	public static void main(String[] args) {
		Juego juego = new Juego();
		juego.iniciar();
			

	}
	

}
